﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TestIdentity.App_Start;
using TestsApplication.Util;

namespace TestsApplication
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DeleteUsers deleteUsers = new DeleteUsers();
            deleteUsers.DeleteNotConfirmedUsers();
            //autofac
            AutofacConfig.ConfigureContainer();
        }
    }
}
