﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using TestsApplication.Interfaces;
using TestsApplication.Models;
using TestsApplication.Repositories;

namespace TestsApplication.Util
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register controllers.
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Register types that expose interfaces...
            builder.RegisterType<TestsRepository>().As<ITests<TestsModel>>().InstancePerRequest();
            builder.RegisterType<QuestionsRepository>().As<IQuestions<QuestionsModel>>().InstancePerRequest();
            builder.RegisterType<AnswersRepository>().As<IAnswers<AnswersModel>>().InstancePerRequest();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}