﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestsApplication.Contexts;
using TestsApplication.Interfaces;
using TestsApplication.Models;

namespace TestsApplication.Repositories
{
    public class TestsRepository : ITests<TestsModel>
    {
        private readonly TestsContext _context;

        public TestsRepository()
        {
            _context = TestsContext.Create();
        }
        public void Dispose()
        {
            _context.Dispose();
        }

        public void Add(TestsModel instance)
        {
            _context.TestsDB.Add(instance);
            _context.SaveChanges();
        }

        public bool Edit(int id, string userName, string message)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id, string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TestsModel> GetTests()
        {
            return _context.TestsDB.ToList();
        }
        public IEnumerable<TestsModel> GetTestsWithQuestions()
        {
            return _context.TestsDB.ToList();
        }
        public void Add(string name, string info)
        {
            var test = new TestsModel
            {
                TestName = name,
                Information = info
            };
            _context.TestsDB.Add(test);
            _context.SaveChanges();
        }
        public void Add(ICollection<QuestionsModel> question, string name, string info)
        {
            var test = new TestsModel
            {
                TestName = name,
                Information = info
            };
            if (_context.TestsDB.Add(test).Questions == null)
            {
                _context.TestsDB.Add(test).Questions = question;
            }
            else
            {
                _context.TestsDB.Add(test).Questions.Add((QuestionsModel)question);
            }
            //var y = _context.TestsDB.Add(test).Questions.FirstOrDefault(x => x.Question == "x").Answers;

            _context.SaveChanges();
        }
        public void Add(string name, string info, QuestionsModel item)
        {
            var test = new TestsModel
            {
                TestName = name,
                Information = info
            };
            _context.TestsDB.Add(test);
            _context.SaveChanges();
            var _test = _context.TestsDB.FirstOrDefault(x => x.TestName == name & x.Information == info);
            _test.Questions.Add(item);
            _context.SaveChanges();

        }

        public IEnumerable<TestsModel> GetTests(string testName)
        {
            throw new NotImplementedException();
        }

        public TestsModel FindByName(string name)
        {
            return _context.TestsDB.FirstOrDefault(x => x.TestName == name);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}