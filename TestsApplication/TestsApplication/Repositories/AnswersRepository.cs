﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TestsApplication.Contexts;
using TestsApplication.Interfaces;
using TestsApplication.Models;

namespace TestsApplication.Repositories
{
    public class AnswersRepository : IAnswers<AnswersModel>
    {
        private readonly TestsContext _context;

        public AnswersRepository()
        {
            _context = TestsContext.Create();
        }

        public void Add(AnswersModel instance)
        {
            _context.AnswersDB.Add(instance);
            _context.SaveChanges();
        }

        public void Add(List<AnswersModel> list)
        {
            _context.AnswersDB.AddRange(list);
            _context.SaveChanges();
        }
        public bool Delete(int id, string question)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public bool Edit(int id, string question, string newquestion)
        {
            throw new NotImplementedException();
        }

        public AnswersModel FindByName(string name)
        {
            throw new NotImplementedException();
        }
    }
}