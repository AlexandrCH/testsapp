﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TestsApplication.Contexts;
using TestsApplication.Interfaces;
using TestsApplication.Models;

namespace TestsApplication.Repositories
{
    public class QuestionsRepository : IQuestions<QuestionsModel>
    {
        private readonly TestsContext _context;

        public QuestionsRepository()
        {
            _context = TestsContext.Create();
        }

        public void Add(string question)
        {
            var instance = new QuestionsModel
            {
                Question = question
            };
            _context.QuestionsDB.Add(instance);
            _context.SaveChanges();
        }

        public void Add(string question, string testName)
        {
            throw new NotImplementedException();
        }
        public void Add(List<QuestionsModel> list)
        {
            _context.QuestionsDB.AddRange(list);
            _context.SaveChanges();
        }
        public bool Delete(int id, string question)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public bool Edit(int id, string question, string newquestion)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<QuestionsModel> GetQuestions()
        {
            var temp =_context.QuestionsDB.Include(p => p.Tests).Where(x => x.Tests.TestName == "TestName").ToList();
            var temp1 = _context.QuestionsDB.Where(x => x.Tests.TestName == "TestName").ToList();

            
            
            return _context.QuestionsDB.ToList();
        }

        public bool CheckTestName(string testName)
        {
            if (_context.QuestionsDB.Where(x => x.Tests.TestName == testName).ToList().Count == 0)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<QuestionsModel> GetQuestions(string testName)
        {
            return _context.QuestionsDB.Where(x => x.Tests.TestName == testName).ToList();
        }

        public QuestionsModel FindByName(string name)
        {
            return _context.QuestionsDB.FirstOrDefault(x => x.Question == name);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Add(string question, string testName, string info)
        {
            var instance = new QuestionsModel
            {
                Question = question,
                Tests = new TestsModel
                {
                    TestName = testName,
                    Information = info
                }
            };
            _context.QuestionsDB.Add(instance);
            _context.SaveChanges();
        }
  
    }
}