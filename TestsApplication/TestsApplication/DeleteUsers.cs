﻿using System;
using System.Diagnostics;
using System.Linq;
using TestsApplication.Models;

namespace TestsApplication
{
    public class DeleteUsers 
    {
        /// <summary>
        /// To delete users that didn't confirme email registration after 24 hours
        /// </summary>
        public void DeleteNotConfirmedUsers()
        {
            System.Timers.Timer timer = new System.Timers.Timer();
            //check every second
            //timer.Interval = 60000;
            //check every 1 hour
            timer.Interval = TimeSpan.FromHours(1).TotalMilliseconds;
            timer.Elapsed += HandlerTimerElapsed;
            timer.Start();
        }

        private void HandlerTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Debug.WriteLine(DateTime.Now);
            using (var dbConnection = ApplicationDbContext.Create())
            {
                var users = dbConnection.Users.Where(x => x.EmailConfirmed == false);
                if (users.Count() != 0)
                {
                    foreach (var item in users)
                    {
                        var currentTime = DateTime.Now;
                        var registrationDate = DateTime.Parse(item.RegistrationDate);
                        if (currentTime > registrationDate.AddDays(1))
                        {
                            using (var db = ApplicationDbContext.Create())
                            {
                                var user = db.Users.Find(item.Id);
                                db.Users.Remove(user);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
        }
    }
}