﻿using System;
using System.Web;
using System.Web.Http.Filters;
using Elmah;

namespace TestsApplication
{
    /// <summary>
    /// Elmah WebApi configuaration
    /// </summary>
    public class ElmahHandleWebApiErrorAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var e = context.Exception;
            RaiseErrorSignal(e);
        }
        /// <summary>
        /// To add exception from WebApi 
        /// </summary>
        /// <param name="e">Exception</param>
        /// <returns></returns>
        private static bool RaiseErrorSignal(Exception e)
        {
            var context = HttpContext.Current;
            if (context == null)
                return false;
            var signal = ErrorSignal.FromContext(context);
            if (signal == null)
                return false;
            signal.Raise(e, context);
            return true;
        }
    }
}