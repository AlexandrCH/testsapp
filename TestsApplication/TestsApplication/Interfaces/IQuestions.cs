﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsApplication.Interfaces
{
    public interface IQuestions<T> : IDisposable where T : class
    {
        void Add( string question);
        void Add(string question, string testName, string info);
        void Save();
        void Add(string question, string testName);

        bool CheckTestName(string testName);

        void Add(List<T> list);

        bool Edit(int id, string question, string newquestion);

        bool Delete(int id, string question);

        T FindByName(string name);

        IEnumerable<T> GetQuestions();

        IEnumerable<T> GetQuestions(string testName);
    }
}
