﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsApplication.Interfaces
{
    public interface IAnswers<T> : IDisposable where T : class
    {
        void Add(List<T> list);

        void Add(T instance);

        bool Edit(int id, string question, string newquestion);

        bool Delete(int id, string question);

        T FindByName(string name);
    }
}
