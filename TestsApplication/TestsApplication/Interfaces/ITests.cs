﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestsApplication.Models;

namespace TestsApplication.Interfaces
{
    public interface ITests<T> : IDisposable where T : class
    {
        void Add( string name, string info);
        void Add(ICollection<QuestionsModel> question, string name, string info);
        void Add(string name, string info, QuestionsModel item);

        void Add(T instance);

        bool Edit(int id, string userName, string message);

        bool Delete(int id, string name);
        void Save();
        IEnumerable<T> GetTests();
        T FindByName(string name);
        IEnumerable<T> GetTests(string testName);
    }
}
