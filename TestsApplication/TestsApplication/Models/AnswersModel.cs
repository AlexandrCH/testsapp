﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestsApplication.Models
{
    [Table("Answers")]
    public class AnswersModel
    {
        [MaxLength(100)]
        public string Answer { get; set; }

        public int Id { get; set; }

        public virtual QuestionsModel Questions { get; set; }
    }
}