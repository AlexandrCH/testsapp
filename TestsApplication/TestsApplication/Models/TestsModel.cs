﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestsApplication.Models
{
    [Table("Tests")]
    public class TestsModel
    {
        [Required]
        [MaxLength(50)]
        public string TestName { get; set; }

        [MaxLength(500)]
        public string Information { get; set; }

        [Key]
        public int Id { get; set; }

        public virtual ICollection<QuestionsModel> Questions { get; set; }
    }
}