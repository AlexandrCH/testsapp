﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestsApplication.Models
{
    [Table("Questions")]
    public class QuestionsModel
    {
        [MaxLength(100)]
        public string Question { get; set; }

        public int Id { get; set; }

        public virtual TestsModel Tests { get; set; }

        public virtual ICollection<AnswersModel> Answers { get; set; }
    }
}