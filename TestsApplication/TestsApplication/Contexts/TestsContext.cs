﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TestsApplication.Models;

namespace TestsApplication.Contexts
{
    public class TestsContext: DbContext
    {
        public TestsContext() : base("TestsConnection") { }

        public DbSet<TestsModel> TestsDB { get; set; }
        public DbSet<QuestionsModel> QuestionsDB { get; set; }

        public DbSet<AnswersModel> AnswersDB { get; set; }
        public static TestsContext Create()
        {
            return new TestsContext();
        }
    }
}