﻿var urlCreateAnswer = "/api/CreateAnswerApi";

$(function () {
    $(function () {
        $("#saveAnswers").click(function () {
            $("#result").text("");
            var answer1 = $("#answerInput1").val();
            var answer2 = $("#answerInput2").val();
            var answer3 = $("#answerInput3").val();
            var question = $("#_name").text();
            var questionId = $("#questionId_").text();
            var dataCollection = {
                Answer1: answer1,
                Answer2: answer2,
                Answer3: answer3,
                Question: question,
                Id: questionId
            };
            $.ajax({
                url: urlCreateAnswer,
                type: "POST",
                data: JSON.stringify(dataCollection),
                contentType: "application/json",
                success: function (xhr, status, error) {
                    $("#result").text("Answers are added!");
                },
                error: function (xhr, status, error) {
                    if (xhr.status == 404) {
                        $("#result").text("Empty fields!");
                    }
                    $("#result").text(xhr.message);
                }
            });
        });
    });
});

