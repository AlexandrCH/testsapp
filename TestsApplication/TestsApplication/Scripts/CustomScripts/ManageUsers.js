﻿var url = "/api/BlockUsersApi";
var urlUnblock = "/api/UnblockUsersApi";


$(function () {
    $(function () {
        $("#blockUser").click(function () {
            $("#result").text("");
            var name = $("#dropDown").val();

            $.ajax({
                url: url,
                type: "POST",
                data: JSON.stringify(name),
                contentType: "application/json",
                success: function (xhr, status, error) {
                    $("#dropDown").val("");
                    $("#result").text("User is blocked!");
                },
                error: function (xhr, status, error) {
                    if (xhr.status == 502) {
                        $("#result").text("Select a user!");
                    }
                    if (xhr.status == 404) {
                        $("#result").text("You couldn't block/unblock 'Administrator'");
                    }
                    $("#result").text(xhr.message);
                }
            });
        });
    });
});

$(function () {

    $(function () {
        $("#unblockUser").click(function () {
            $("#result").text("");
            var name = $("#dropDown").val();

            $.ajax({
                url: urlUnblock,
                type: "POST",
                data: JSON.stringify(name),
                contentType: "application/json",
                success: function (xhr, status, error) {
                    $("#dropDown").val("");
                    $("#result").text("User is unblocked!");
                },
                error: function (xhr, status, error) {
                    if (xhr.status == 502) {
                        $("#result").text("Select a user!");
                    }
                    if (xhr.status == 404) {
                        $("#result").text("You couldn't block/unblock 'Administrator'");
                    }
                    $("#result").text(xhr.message);

                }
            });
        });
    });
});


function Unblock() {
    $("#result").text("");
    var name = $("#dropDown").val();

    $.ajax({
        url: urlUnblock,
        type: "POST",
        data: JSON.stringify(name),
        contentType: "application/json",
        success: function (xhr, status, error) {
            $("#result").text("User is unblocked!");
        },
        error: function (xhr, status, error) {
            if (xhr.status == 502) {
                $("#result").text("Select a user!");
            }
            if (xhr.status == 404) {
                $("#result").text("You couldn't block/unblock 'Administrator'");
            }
            $("#result").text(xhr.message);
        }
    });
}


function Block() {

    $("#result").text("");
    var name = $("#dropDown").val();

    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(name),
        contentType: "application/json",
        success: function (xhr, status, error) {
            $("#result").text("User is blocked!");
        },
        error: function (xhr, status, error) {
            if (xhr.status == 502) {
                $("#result").text("Select a user!");
            }
            if (xhr.status == 404) {
                $("#result").text("You couldn't block/unblock 'Administrator'");
            }
            $("#result").text(xhr.message);
        }
    });
}


$(function () {
    $('#toggleBlockUnblock').change(function () {

        if ($(this).prop('checked')) {
            Unblock();
        } else
            Block();
    });
});
