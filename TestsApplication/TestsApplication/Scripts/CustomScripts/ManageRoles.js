﻿var urlAddRoles = "/api/AddRolesApi";
var urlDeleteRoles = "/api/DeleteRolesApi";

//Add Role to User
$(function () {
    $(function () {
        $("#addRole").click(function () {
            $("#result").text("");
            var user = $("#usersList").val();
            var role = $("#rolesList").val();
            var dataCollection = {
                UserName: user, UserRole: role
            }
            $.ajax({
                url: urlAddRoles,
                type: "POST",
                data: JSON.stringify(dataCollection),
                contentType: "application/json",
                success: function (xhr, status, error) {
                    $("#result").text("Role is added!");
                },
                error: function (xhr, status, error) {
                    if (xhr.status == 502) {
                        $("#result").text("Select User and Role!");
                    }
                    if (xhr.status == 403) {
                        $("#result").text("User already has this role!");
                    }
                    $("#result").text(xhr.message);
                }
            });
        });
    });
});

//Delete Role from User
$(function () {
    $(function () {
        $("#deleteRole").click(function () {
            $("#result").text("");
            var user = $("#usersList").val();
            var role = $("#rolesList").val();
            var dataCollection = {
                UserName: user, UserRole: role
            }
            $.ajax({
                url: urlDeleteRoles,
                type: "POST",
                data: JSON.stringify(dataCollection),
                contentType: "application/json",
                success: function (xhr, status, error) {
                    $("#result").text("Role is deleted!");
                },
                error: function (xhr, status, error) {
                    if (xhr.status == 502) {
                        $("#result").text("Select User and Role!");
                    }
                    if (xhr.status == 403) {
                        $("#result").text("User doesn't have this role!");
                    }
                    if (xhr.status == 406) {
                        $("#result").text("You couldn't delete your own 'Administrator' role!!!");
                    }
                    if (xhr.status == 409) {
                        $("#result").text("You couldn't delete last role of user! User must have at least one role!");
                    }
                    $("#result").text(xhr.message);
                }
            });
        });
    });
});

