﻿var urlChangeUserPass = "/api/ChangeUserPasswordApi";

$(function () {

    $(function () {
        $("#ChangePass").click(function () {
            $("#resultPass").text("");
            var password = $("#Password").val();
            var email = $("#Email").val();
            var dataCollection = {
                  Email: email, Password : password
            }
            $.ajax({
                url: urlChangeUserPass,
                type: "POST",

                data: JSON.stringify(dataCollection),

                contentType: "application/json",
                success: function (xhr, status, error) {
                    $("#resultPass").text("Password is changed!!!");
                },
                error: function (xhr, status, error) {
                   
                    if (xhr.status == 404) {
                        $("#resultPass").text("It is empty field!");
                    }
                }
            });
        });
    });
});


