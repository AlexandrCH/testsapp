﻿var urlChangeUserProf = "/api/ChangeUserProfApi";

$(function () {

    $(function () {
        $("#SaveChanges").click(function () {
            $("#result").text("");
            var fullName = $("#FullName").val();
            var userName = $("#User_Name").val();
            var email = $("#Email").val();
            var dataCollection = {
                Name: userName, FullName: fullName,  Email: email
            }
            $.ajax({
                url: urlChangeUserProf,
                type: "POST",

                data: JSON.stringify(dataCollection),

                contentType: "application/json",
                success: function (xhr, status, error) {
                    $("#result").text("Profile is changed!!!");
                },
                error: function (xhr, status, error) {
                    if (xhr.status == 502) {
                        $("#result").text("It is the same value!");
                    }
                    if (xhr.status == 404) {
                        $("#result").text("Shouldn't be empty fields!");
                    }
                    if (xhr.status == 409) {
                        $("#result").text(xhr.statusText);
                    }
                }
            });
        });
    });
});


