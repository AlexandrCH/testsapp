﻿var urlManage = "/api/ManageApi";

$(function () {

    $(function () {
        $("#changeButton").click(function () {
            $("#result").text("");
            var fullName = $("#fullName").val();
            var userName = $("#user_Name").val();
            var dataCollection = {
                UserName: userName, FullName: fullName
            }
            $.ajax({
                url: urlManage,
                type: "POST",

                data: JSON.stringify(dataCollection),

                contentType: "application/json",
                success: function (xhr, status, error) {
                    $("#result").text("FullName is changed!!!");
                },
                error: function (xhr, status, error) {
                    if (xhr.status == 502) {
                        $("#result").text("It is the same value!");
                    }
                    if (xhr.status == 404) {
                        $("#result").text("Empty field!");
                    }
                }
            });
        });
    });
});


