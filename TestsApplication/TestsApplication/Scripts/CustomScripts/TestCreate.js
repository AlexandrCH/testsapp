﻿var url2 = "/api/CreateTestApi";

//Create a new Questions
$(function () {
    $(function () {
        $("#addQuestions").click(function () {
            $("#result").text("");
            var question1 = $("#questionInput1_").val();
            var question2 = $("#questionInput2_").val();
            var question3 = $("#questionInput3_").val();
            var name = $("#testNameInput_").val();
            var testId = $("#testId").text();

            var info = $("#infoInput_").val();

            var question = [{
                Question: question1,
            }]


            if (question1 == "" || name == "" || info == "") {
                //$("#result").text("All fields are required!");
                alert("All fields are required!");
            }
            else {
                $.ajax({
                    url: url2,
                    type: "POST",

                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        TestName: name,
                        Information: info,
                        Questions: question
                    }),

                    success: function (xhr, status, error) {
                        $("#result").text("ok!");
                        window.location.href = "http://10.10.20.45:8099/Tests/Answers" + "/" + name;
                    },
                    error: function (xhr, status, error) {
                        if (xhr.status == 404) {
                            $("#result").text("Empty fields!");
                        } if (xhr.status == 502) {
                            //$("#result").text("Test name already exists! Please select another TestName");
                            alert("TestName already exists! Please select another TestName");
                        }
                        $("#result").text(xhr.message);
                    }
                });
            }
        });
    });
});

$(function () {
    $(function () {
        $("#create_Test").click(function () {
            var name = $("#testNameInput_").val();
            var info = $("#infoInput_").val();
            if (name != "" & info != "") {
                $("#result").text("");
                $("#questionsForm").show();
            }
            else {
                $("#questionsForm").hide();
                $("#result").text("All fields are required!");

            }
        });
    });
});


function hide() {
    $("#questionsForm").hide();
}
window.onload = hide;