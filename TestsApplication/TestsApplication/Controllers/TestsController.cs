﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Newtonsoft.Json;
using TestsApplication.Interfaces;
using TestsApplication.Models;
using System;
using System.Collections.Generic;

namespace TestsApplication.Controllers
{
    [Authorize]
    public class TestsController : Controller
    {
        private ITests<TestsModel> testsRepository;
        private IQuestions<QuestionsModel> questionsRepository;


        public TestsController(ITests<TestsModel> testsRepository, IQuestions<QuestionsModel> questionsRepository)
        {
            this.testsRepository = testsRepository;
            this.questionsRepository = questionsRepository;
        }

        public ActionResult Index()
        {
            return View(testsRepository.GetTests());
        }

        [Authorize(Roles = "Tutor,Advanced tutor")]
        public ActionResult CreateTest()
        {
            return View();
        }
       

        public ActionResult Questions()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }
        public ActionResult Answers()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Answers(string id)
        {
            return View(questionsRepository.GetQuestions(id));
        }
        public ActionResult AddAnswers()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult AddAnswers(string x, string y)
        {
            var list = new List<String>();
            list.Add(y);
            ViewBag.Name = y;
            return View(list);
        }
    }
}