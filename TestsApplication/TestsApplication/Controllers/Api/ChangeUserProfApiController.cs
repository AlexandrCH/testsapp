﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using TestsApplication.Models;

namespace TestsApplication.Controllers.Api
{
    [Authorize]
    public class ChangeUserProfApiController : ApiController
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set { _userManager = value; }
        }

        public ApplicationRoleManager RoleManager
        {
            get { return _roleManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>(); }
            private set { _roleManager = value; }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(JObject data)
        {
            var instance = data.ToObject<ChangeUserProfileViewModel>();
            if ( !string.IsNullOrEmpty(instance.Name) || !string.IsNullOrEmpty(instance.FullName))
            {

                var user = await UserManager.FindByEmailAsync(instance.Email);

                user.UserName = instance.Name;
                user.FullName = instance.FullName;

                IdentityResult result = await UserManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    var responseMsg = new HttpResponseMessage(HttpStatusCode.Accepted);
                    return responseMsg;
                }
                return new HttpResponseMessage(HttpStatusCode.Conflict) { ReasonPhrase = AddErrors(result) };
            }
            else
            {
                var responseMsg = new HttpResponseMessage(HttpStatusCode.NotFound);
                return responseMsg;
            }
        }

        private string AddErrors(IdentityResult result)
        {
            string temp = String.Empty;
            foreach (var error in result.Errors)
            {
                 temp += error + " "; 
            }
            return temp;
        }
    }
}