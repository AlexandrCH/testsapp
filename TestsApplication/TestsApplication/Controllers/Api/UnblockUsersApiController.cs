﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace TestsApplication.Controllers.Api
{
    [Authorize]
    public class UnblockUsersApiController : ApiController
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        [HttpPost]
        public HttpResponseMessage Post([FromBody] string value)
        {
            var user = UserManager.Users.FirstOrDefault(x => x.UserName == value);
            if (!string.IsNullOrEmpty(value))
            {
                if (value != "admin")
                {
                    user.LockoutEndDateUtc = null;
                    UserManager.Update(user);

                    return new HttpResponseMessage(HttpStatusCode.Created);

                }
                return new HttpResponseMessage(HttpStatusCode.NotFound);

            }

            return new HttpResponseMessage(HttpStatusCode.BadGateway);
        }
    }
}
