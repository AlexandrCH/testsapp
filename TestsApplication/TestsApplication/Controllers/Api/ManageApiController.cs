﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using TestsApplication.Models;

namespace TestsApplication.Controllers.Api
{
    [Authorize]
    public class ManageApiController : ApiController
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public HttpResponseMessage Post(JObject data)
        {
            var instance = data.ToObject<UserNameFullNameViewModel>();
            if (!string.IsNullOrEmpty(instance.UserName))
            {
                var fullNameDb = UserManager.FindByName(instance.UserName).FullName;

                if (instance.FullName == fullNameDb)
                    return new HttpResponseMessage(HttpStatusCode.BadGateway);
                var user = UserManager.FindByName(instance.UserName);
                user.FullName = instance.FullName;
                UserManager.Update(user);

                var responseMsg = new HttpResponseMessage(HttpStatusCode.Accepted);
                return responseMsg;
            }
            else
            {
                var responseMsg = new HttpResponseMessage(HttpStatusCode.NotFound);
                return responseMsg;
            }
        }
    }
}
