﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using TestsApplication.Interfaces;
using TestsApplication.Models;
using System;

namespace TestsApplication.Controllers.Api
{
    [Authorize]
    public class CreateAnswerApiController : ApiController
    {
        private ITests<TestsModel> testsRepository;
        private IQuestions<QuestionsModel> questionsRepository;
        private IAnswers<AnswersModel> answersRepository;
        public CreateAnswerApiController(ITests<TestsModel> testsRepository, IQuestions<QuestionsModel> questionsRepository, IAnswers<AnswersModel> answersRepository)
        {
            this.testsRepository = testsRepository;
            this.questionsRepository = questionsRepository;
            this.answersRepository = answersRepository;
        }

        [HttpPost]
        public HttpResponseMessage CreateAnswer(JObject data)
        {
            if (data != null)
            {
                var questionsInstance = questionsRepository.FindByName(data["Question"].ToString());

                //QuestionsModel questionsInstance = new QuestionsModel
                //{
                //    Question = data["Question"].ToString(),
                //};

                //questionsRepository.Add(data["Question"].ToString());

                var answerInstance1 = new AnswersModel { Answer = data["Answer1"].ToString() };
                var answerInstance2 = new AnswersModel { Answer = data["Answer2"].ToString() };
                var answerInstance3 = new AnswersModel { Answer = data["Answer3"].ToString() };
                questionsInstance.Answers.Add(answerInstance1);
                questionsInstance.Answers.Add(answerInstance2);
                questionsInstance.Answers.Add(answerInstance3);
                questionsRepository.Save();
                //var answerInstance1 = new AnswersModel { Answer = data["Answer1"].ToString(), Questions = questionsInstance };
                //var answerInstance2 = new AnswersModel { Answer = data["Answer2"].ToString(), Questions = questionsInstance };
                //var answerInstance3 = new AnswersModel { Answer = data["Answer3"].ToString(), Questions = questionsInstance };
                //answersRepository.Add(new List<AnswersModel> { answerInstance1, answerInstance2, answerInstance3 });

                return new HttpResponseMessage(HttpStatusCode.Accepted);
            }
            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }


        [HttpGet]
        [Route("api/books")]
        public HttpResponseMessage Answer(JObject data)
        {
            if (data != null)
            {
                var questionsInstance = questionsRepository.FindByName(data["Question"].ToString());

                //QuestionsModel questionsInstance = new QuestionsModel
                //{
                //    Question = data["Question"].ToString(),
                //};

                //questionsRepository.Add(data["Question"].ToString());

                var answerInstance1 = new AnswersModel { Answer = data["Answer1"].ToString(), Questions = questionsInstance };
                var answerInstance2 = new AnswersModel { Answer = data["Answer2"].ToString(), Questions = questionsInstance };
                var answerInstance3 = new AnswersModel { Answer = data["Answer3"].ToString(), Questions = questionsInstance };
                answersRepository.Add(new List<AnswersModel> { answerInstance1, answerInstance2, answerInstance3 });

                return new HttpResponseMessage(HttpStatusCode.Accepted);
            }
            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }

        [HttpGet]
        [Route("api/test")]
        public HttpResponseMessage BAnswer(JObject data)
        {
            if (data != null)
            {
                var questionsInstance = questionsRepository.FindByName(data["Question"].ToString());

                //QuestionsModel questionsInstance = new QuestionsModel
                //{
                //    Question = data["Question"].ToString(),
                //};

                //questionsRepository.Add(data["Question"].ToString());

                var answerInstance1 = new AnswersModel { Answer = data["Answer1"].ToString() };
                var answerInstance2 = new AnswersModel { Answer = data["Answer2"].ToString() };
                var answerInstance3 = new AnswersModel { Answer = data["Answer3"].ToString() };
                questionsInstance.Answers.Add(answerInstance1);
                questionsInstance.Answers.Add(answerInstance2);
                questionsInstance.Answers.Add(answerInstance3);
                questionsRepository.Save();
                //var answerInstance1 = new AnswersModel { Answer = data["Answer1"].ToString(), Questions = questionsInstance };
                //var answerInstance2 = new AnswersModel { Answer = data["Answer2"].ToString(), Questions = questionsInstance };
                //var answerInstance3 = new AnswersModel { Answer = data["Answer3"].ToString(), Questions = questionsInstance };
                //answersRepository.Add(new List<AnswersModel> { answerInstance1, answerInstance2, answerInstance3 });

                return new HttpResponseMessage(HttpStatusCode.Accepted);
            }
            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }
        [HttpPost]
        [Route("api/test2")]
        public string qqqq(QuestionsModel data)
        {
            var test = data.Id;
            var test1 = data.Question;
            var test2 = data.Tests;


            return test1;
        }
    }
}
