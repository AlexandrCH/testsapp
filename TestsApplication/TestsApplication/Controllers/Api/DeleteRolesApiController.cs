﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using TestsApplication.Models;

namespace TestsApplication.Controllers.Api
{
    [Authorize]
    public class DeleteRolesApiController : ApiController
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set { _userManager = value; }
        }

        public ApplicationRoleManager RoleManager
        {
            get { return _roleManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>(); }
            private set { _roleManager = value; }
        }

        [HttpPost]
        public HttpResponseMessage Post(JObject data)
        {
            var instance = data.ToObject<AddDeleteRolesViewModel>();

            if ( !string.IsNullOrEmpty(instance.UserRole) &
                !string.IsNullOrEmpty(instance.UserName))
            {
                var user = UserManager.Users.FirstOrDefault(x => x.UserName == instance.UserName);
                var currentUser = User.Identity.GetUserName();
                
                if (user != null)
                {
                    if (currentUser == instance.UserName & instance.UserRole == "Administrator")
                    {
                        return new HttpResponseMessage(HttpStatusCode.NotAcceptable);
                    }
                    if (UserManager.GetRoles(user.Id).Contains(instance.UserRole))
                    {
                        if (UserManager.GetRoles(user.Id).Count() == 1)
                        {
                            return new HttpResponseMessage(HttpStatusCode.Conflict);
                        }
                        UserManager.RemoveFromRole(user.Id, instance.UserRole);
                        return new HttpResponseMessage(HttpStatusCode.Accepted);
                    }
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.BadGateway);
        }
    }
}