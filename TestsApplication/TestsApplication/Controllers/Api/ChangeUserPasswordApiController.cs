﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using TestsApplication.Models;

namespace TestsApplication.Controllers.Api
{
    [Authorize]
    public class ChangeUserPasswordApiController : ApiController
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set { _userManager = value; }
        }

        public ApplicationRoleManager RoleManager
        {
            get { return _roleManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>(); }
            private set { _roleManager = value; }
        }

        [HttpPost]
        [Route("api/user/changePass")]
        public async Task<HttpResponseMessage> Post(JObject data)
        {
            var instance = data.ToObject<ChangeUserPasswordViewModel>();
            if ( !string.IsNullOrEmpty(instance.Email) && !string.IsNullOrEmpty(instance.Password))
            {
                var user = await UserManager.FindByEmailAsync(instance.Email);
                var token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                await UserManager.ResetPasswordAsync(user.Id, token, instance.Password);
                //await UserManager.UpdateAsync(user);

                var responseMsg = new HttpResponseMessage(HttpStatusCode.Accepted);
                return responseMsg;
            }
            else
            {
                var responseMsg = new HttpResponseMessage(HttpStatusCode.NotFound);
                return responseMsg;
            }
        }
    }
}