﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using TestsApplication.Interfaces;
using TestsApplication.Models;
using System;

namespace TestsApplication.Controllers.Api
{
    [Authorize]
    public class TestApiController : ApiController
    {
        private ITests<TestsModel> testsRepository;
        private IQuestions<QuestionsModel> questionsRepository;
        public TestApiController(ITests<TestsModel> testsRepository, IQuestions<QuestionsModel> questionsRepository)
        {
            this.testsRepository = testsRepository;
            this.questionsRepository = questionsRepository;
        }

        [HttpPost]
        public HttpResponseMessage CreateTest(JObject data)
        {
            if (data != null)
            {
                if (!questionsRepository.CheckTestName(data["TestName"].ToString()))
                {
                    TestsModel testsInstance = new TestsModel
                    {
                        TestName = data["TestName"].ToString(),
                        Information = data["Information"].ToString()
                    };

                    //testsRepository.Add(testsInstance);

                    var q1 = new QuestionsModel { Question = data["Question1"].ToString(), Tests = testsInstance };
                    var q2 = new QuestionsModel { Question = data["Question2"].ToString(), Tests = testsInstance };
                    var q3 = new QuestionsModel { Question = data["Question3"].ToString(), Tests = testsInstance };
                    questionsRepository.Add(new List<QuestionsModel> { q1, q2, q3 });

                    return new HttpResponseMessage(HttpStatusCode.Accepted);

                }
                return new HttpResponseMessage(HttpStatusCode.BadGateway);
            }
            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }

        [HttpPost]
        [Route("api/CreateTestApi")]
        public HttpResponseMessage CreateTest1(TestsModel data)
        {
            if (data != null)
            {
                testsRepository.Add(data.Questions, data.TestName, data.Information);

                return new HttpResponseMessage(HttpStatusCode.Accepted);
            }
            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }
    }
}
