﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using TestsApplication.Models;

namespace TestsApplication.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ManageController()
        {
        }
        
        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
        }
        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
           
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";

            var userId = User.Identity.GetUserId();

           var model = new EditProfileViewModel
            {
                Email = await UserManager.GetEmailAsync(userId),
                Name = User.Identity.GetUserName(),
                Password = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>()
                    .FindById(User.Identity.GetUserId()).PasswordHash,
                FullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>()
                    .FindById(User.Identity.GetUserId()).FullName,
                Roles = (List<string>)await UserManager.GetRolesAsync(User.Identity.GetUserId())
            };

            return View(model);
        }


        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }


        [Authorize(Roles = "Administrator")]
        public ActionResult AddUsers()
        {
            var roles = RoleManager.Roles.ToList();
            ViewBag.Roles = GetSelectListRoles(roles);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> AddUsers(AddUserViewModel model)
        {
            var roles = RoleManager.Roles.ToList();
            ViewBag.Roles = GetSelectListRoles(roles);

            if (ModelState.IsValid)
            {
               
                model.RegistrationDate = DateTime.Now.Day.ToString();

                var role = Request["Roles"];
                if (!String.IsNullOrEmpty(role))
                {
                    if (role == "Administrator")
                    {
                        ModelState.AddModelError("", "You couldn't use 'Administrator' role!");
                        return View();
                    }
                    ApplicationUser user = new ApplicationUser { UserName = model.Name, Email = model.Email, FullName = model.FullName };
                    user.EmailConfirmed = true;
                    user.RegistrationDate = DateTime.Now.ToString();
                    IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        IdentityResult resultAddToRole = UserManager.AddToRole(user.Id, role);

                        if (resultAddToRole.Succeeded)
                        {
                            ViewBag.Message = "User is created!";
                            return View();
                        }
                        AddErrors(result);
                        return View();
                    }
                  
                    AddErrors(result);
                    return View();
                }
                else
                {
                    ModelState.AddModelError("","Select a Role!");
                    return View();
                }
            }

            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult ManageUsers()
        {
            var users = UserManager.Users.ToList();
            ViewBag.Users = GetSelectListUsers(users);
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult ManageRoles()
        {
            var users = UserManager.Users.ToList();
            ViewBag.Users = GetSelectListUsers(users);
            var roles = RoleManager.Roles.ToList();
            ViewBag.Roles = GetSelectListRoles(roles);
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult EditUsersProfile()
        {
            var users = UserManager.Users.ToList();
            ViewBag.Users = GetSelectListUsers(users);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> EditUsersProfile(string Users)
        {
            if (string.IsNullOrEmpty(Users))
            {
                var users = UserManager.Users.ToList();
                ViewBag.Users = GetSelectListUsers(users);
                ViewBag.Message = "Select user!!!";
                return View();

            }
            var user = UserManager.Users.FirstOrDefault(x => x.UserName == Users);
            var userId = user.Id;
            var model = new EditProfileViewModel
            {
                Email = await UserManager.GetEmailAsync(userId),
                Name = Users,
                Password = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>()
                    .FindById(userId).PasswordHash,
                FullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>()
                    .FindById(userId).FullName,
                Roles = (List<string>)await UserManager.GetRolesAsync(userId)
            };

            return View("UsersProfile", model);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult UsersProfile()
        {
            var users = UserManager.Users.ToList();
            ViewBag.Users = GetSelectListUsers(users);

            return View();
        }

       
        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }


        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            Error
        }
        private IEnumerable<SelectListItem> GetSelectListRoles(IEnumerable<IdentityRole> elements)
        {

            var selectList = new List<SelectListItem>();


            foreach (var element in elements)
            {

                selectList.Add(new SelectListItem
                {
                    Value = element.Name,
                    Text = element.Name
                });
            }

            return selectList;
        }
        private IEnumerable<SelectListItem> GetSelectListUsers(IEnumerable<ApplicationUser> elements)
        {

            var selectList = new List<SelectListItem>();


            foreach (var element in elements)
            {

                selectList.Add(new SelectListItem
                {
                    Value = element.UserName,
                    Text = element.UserName
                });
            }

            return selectList;
        }

        #endregion
    }
}