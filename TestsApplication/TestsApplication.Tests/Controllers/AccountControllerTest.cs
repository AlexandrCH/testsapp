﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestsApplication;
using TestsApplication.Controllers;

namespace TestsApplication.Tests.Controllers
{
    [TestClass]
    public class AccountControllerTest
    {
        [TestMethod]
        public void Login()
        {
            AccountController controller = new AccountController();

            ViewResult result = controller.Login("url") as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Register()
        {
            AccountController controller = new AccountController();

            ViewResult result = controller.Register() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ConfirmEmail()
        {
            AccountController controller = new AccountController();

            var result = controller.ConfirmEmail("Id","Code");

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ForgotPassword()
        {
            AccountController controller = new AccountController();

            var result = controller.ForgotPassword() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ForgotPasswordConfirmation()
        {
            AccountController controller = new AccountController();

            var result = controller.ForgotPasswordConfirmation() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ResetPassword()
        {
            AccountController controller = new AccountController();

            var result = controller.ResetPassword("Code") as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ResetPasswordConfirmation()
        {
            AccountController controller = new AccountController();

            var result = controller.ResetPasswordConfirmation() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Edit()
        {
            AccountController controller = new AccountController();

            Task<ActionResult> result = controller.Edit() as Task<ActionResult>;

            Assert.IsNotNull(result);
        }

    }
}