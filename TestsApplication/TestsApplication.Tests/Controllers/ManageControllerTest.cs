﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestsApplication;
using TestsApplication.Controllers;

namespace TestsApplication.Tests.Controllers
{
    [TestClass]
    public class ManageControllerTest
    {
        [TestMethod]
        public void Index()
        {
            ManageController controller = new ManageController();

            Task<ActionResult> result = controller.Index(ManageController.ManageMessageId.SetPasswordSuccess) as Task<ActionResult>;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ChangePassword()
        {
            ManageController controller = new ManageController();

            ViewResult result = controller.ChangePassword() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void SetPassword()
        {
            ManageController controller = new ManageController();

            ViewResult result = controller.SetPassword() as ViewResult;

            Assert.IsNotNull(result);
        }

        //[TestMethod]
        //public void AddUsers()
        //{
        //    ManageController controller = new ManageController();

        //    ViewResult result = controller.AddUsers() as ViewResult;

        //    Assert.IsNotNull(result);
        //}

        //[TestMethod]
        //public void ManageUsers()
        //{
        //    ManageController controller = new ManageController();

        //    ViewResult result = controller.ManageUsers() as ViewResult;

        //    Assert.IsNotNull(result);
        //}

        //[TestMethod]
        //public void EditUsersProfile()
        //{
        //    ManageController controller = new ManageController();

        //    ViewResult result = controller.EditUsersProfile() as ViewResult;

        //    Assert.IsNotNull(result);
        //}

        //[TestMethod]
        //public void UsersProfile()
        //{
        //    ManageController controller = new ManageController();

        //    ViewResult result = controller.UsersProfile() as ViewResult;

        //    Assert.IsNotNull(result);
        //}


    }
}